'use strict';
const session = require('express-session');
const req = require('express/lib/request');
const { TokenExpiredError } = require('jsonwebtoken');
const { user } = require('pg/lib/defaults');
const {
  Model
} = require('sequelize');
const authentication = require('../middleware/auth');
const authors = require('./authors');
module.exports = (sequelize, DataTypes) => {
  class Reseps extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reseps.belongsTo(models.Authors)

    }

    static async dashboardData(){
      try {
        const data = await this.findAll();
        return data;
      } catch (error) {
        Promise.reject(error);
      }
    }

    static async oneResepData(resepId){
      try {
        let data = await this.findOne({
          where: {
              id: resepId
          },
        });
        return data;
      } catch (error) {
        Promise.reject(error);
      }
    }

    static async editOneResepData(resepId, data){
      try {
        const resep = await this.findOne({
          where: {
              id: resepId
          }
        });
        if(resep){
          await resep.update({
            title: data.title,
            bahan: data.bahan,
            cara: data.cara
          });
        }
        return resep;   
      } catch (error) {
        Promise.reject(error);
      }
    }

    static async tambah(data){
      try {
        console.log(data);
          await this.create({
          title: data.data.title, 
          bahan: data.data.bahan,
          cara: data.data.cara,
          AuthorId : data.userId,
        });
      } catch (error) {
        Promise.reject(error)
      }
    } 

    static async deleteOneResepData(resepId){
      try {
        const resepData = await this.findOne({
          where: {
              id: resepId
          }
        });
        if(resepData){
          await resepData.destroy();
        }
        return resepData;
      } catch (error) {
        Promise.reject(error);
      }
    }

  }
  Reseps.init({
    title: DataTypes.STRING,
    bahan: DataTypes.STRING,
    cara: DataTypes.STRING,
    AuthorId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Reseps',
  });
  return Reseps;
};