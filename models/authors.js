'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

module.exports = (sequelize, DataTypes) => {
  class Authors extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Authors.hasMany(models.Reseps)
    }

    static #hashPassword(password){
      return bcrypt.hashSync(password, 10)
    }

    static #comparepassword(password, dbpassword){
     let ispasswordValid = bcrypt.compareSync(password, dbpassword)
     return ispasswordValid;
    }

    static #generateToken(payload){
      let data = {
        id : payload.id,
        email : payload.email,
      }
      const secret = "My Secret";
      let token = jwt.sign(data, secret);
      return token;
    }

    static async register(data){
      const encrypted = this.#hashPassword(data.password)
      try {
          await this.create({
          email: data.email, 
          password: encrypted,
          username: data.username
        });
      } catch (error) {
        Promise.reject(error)
      }
    } 

    static async login(data){
      try {
        let author = await this.findOne({
          where: {
            email : data.email,
          },
        });
        const resultObj = {
          success : false,
          message : "",
          token : ""
        }
        if (!author){
          resultObj.message = "Periksa kembali data anda!"
        } else{
          let dbpassword = author.dataValues.password;
          let ispasswordValid = this.#comparepassword(
            data.password, 
            dbpassword
            );
          if (!ispasswordValid){
            resultObj.message = "Password Salah!"
          } else {
            resultObj.token = this.#generateToken(author.dataValues)
            resultObj.success = true;
            resultObj.data = author.dataValues
          }
        }
        return Promise.resolve(resultObj);

      } catch (error) {
        Promise.reject(error)
      }
    }

    static autentikasi(token){
      const secret = "My Secret";
      let data = jwt.verify(token, secret)
      return data;
    }

  }
  Authors.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    username: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Authors',
  });
  return Authors;
};