const {Authors} = require("../models")


function authentication(req, res, next){

    const authorization = req.headers.authorization;
    if(!authorization){
        res.status(401).json({
            message: "Silahkan login",
        })
    } else {
        let data = Authors.autentikasi(authorization)
        req.dataAuthor = data;
        next()
    }

}

module.exports = authentication