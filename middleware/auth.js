function authentication(req, res, next){

    const user = req.session.user;
    if (!user) {
        res.redirect("/masuk")
    } else {
    next()
    }
   
}

module.exports = authentication