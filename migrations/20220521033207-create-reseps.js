'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Reseps', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      bahan: {
        allowNull: false,
        type: Sequelize.STRING
      },
      cara: {
        allowNull: false,
        type: Sequelize.STRING
      },
      AuthorId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "Authors",
          key:"id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Reseps');
  }
};