const route = require("express").Router()
const { getRegister } = require("../controllers/authorscontrollers.js");
const AuthorsController = require("../controllers/authorscontrollers.js")
const auth = require("../middleware/authentikasi")
const authen = require ("../middleware/auth");
const ResepsController = require("../controllers/resepscontrollers.js");

route.get("/register", AuthorsController.getRegister);
route.post("/register", AuthorsController.PostRegister);
route.get("/masuk", AuthorsController.getLogin)
route.post("/masuk", AuthorsController.login)

route.get("/dashboard", authen, ResepsController.getDashboard)

route.get("/add", authen, ResepsController.getAddResep)
route.post("/add", authen, ResepsController.PostResep)

route.get("/edit/:resepId", authen, ResepsController.getEditResep)
route.post("/edit/:resepId", authen, ResepsController.postEditResep)

route.get("/view/:resepId", authen, ResepsController.getReadResep)

route.post("/delete/:resepId", authen, ResepsController.postDeleteResep)

route.get("/who", auth, (req,res)=>{
    const data = req.dataAuthor;
    res.status(200).json({data})
});


module.exports = route;