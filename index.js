const express = require("express")
const cors = require("cors")
const app = express()
const PORT = 8000
const router = require("./routes");
const session = require("express-session")


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/views"));
app.set("view engine", "ejs");

app.use(session({
    secret : "My Secret",
    isLogin : false,
}))
app.use(router);

app.listen(PORT, () => {
    console.log("Server runnning on port", PORT);
});