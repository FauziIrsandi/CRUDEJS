const { Reseps } = require("../models");

class ResepsController {

    static async getDashboard(req, res){
        try {
            let data = await Reseps.dashboardData();
            res.render("dashboard-resep", {
                data: data
            })   
        } catch (error) {
            
        }
    }

    static getAddResep(req,res){
        res.render("db")
    }

    static GetAllReseps(req,res){
        Reseps.findAll()
        .then(result=> {
            res.status(200).json(result)
        })
        .catch(error => {
            res.status(500).json({
                message: "internal server error"
            })
        })
    }

        static async PostResep(req,res){
            // const data = req.body;
            const data = {
                data : req.body,
                userId: req.session.user.id
            }
            // req.session.data=data;
            // console.log(req.session)
            try {
                await Reseps.tambah(data)
                res.redirect("/dashboard")
            } catch (error) {
                console.log(error)
                };
        }

        static async getReadResep(req,res){
            try {
                let data = await Reseps.oneResepData(req.params.resepId);
                if(data){
                    res.render("db", {
                        session: req.session,
                        uri: "read",
                        data: data,
                    });
                } else {
                    res.redirect("/dashboard")
                }
            } catch (error) {
                console.log(error)
            }
        }

        static async getEditResep(req,res){
            try {
                let data = await Reseps.oneResepData(req.params.resepId);
                if(data){
                    res.render("db", {
                        session: req.session,
                        uri: "edit",
                        data: data,
                    });
                } else {
                    res.redirect("/dashboard")
                }
            } catch (error) {
                console.log(error)
            }
        }

        static async postEditResep(req,res){
            try {
                const data = req.body;
                await Reseps.editOneResepData(req.params.resepId, data)
                res.redirect("/dashboard")
            } catch (error) {
                console.log(error)
            }
        }

        static async postDeleteResep(req,res){
            try {
                let data = await Reseps.deleteOneResepData(req.params.resepId);
                res.redirect("/dashboard")
            } catch (error) {
                console.log(error)
            }
        }
}

module.exports = ResepsController