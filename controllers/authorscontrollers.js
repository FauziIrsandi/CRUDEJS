const {Authors} = require("../models")

let message = ""

class AuthorsController {

static getRegister(req,res){
    res.render("register")
}

static async PostRegister(req,res){
    const data = req.body;
    try {
        await Authors.register(data)
        res.redirect("/masuk")
    } catch (error) {
        console.log(error)
        };
    }

static getLogin(req,res){
    res.render("login", {
        message: message,
    })
}
static async login(req,res){
    const data = req.body;
    try {
        let token = await Authors.login(data);
        if(!token.success){    
            message = token.message
            res.redirect("/masuk")
        } else {
            message = "";
            const user = {
                id : token.data.id,
            }
            req.session.user = user;
            console.log(req.session)
            res.redirect("/dashboard")
        }
    } catch (error) {
        console.log(error);
    }
}

static getDashboard(req,res){
    res.render("db")
}

}

module.exports = AuthorsController;